import React, { Component } from "react"
import { connect } from "react-redux"
import { setWeekly, setHourly } from "./actions"

const WUNDERGROUND_KEY = "c6b91e0d2867534d";

class Weather extends Component {
  componentDidMount() {
    this.initMap()
  }

  initMap() {
    let input = document.getElementById("location")
    this.autocomplete = new google.maps.places.Autocomplete(input, {types: ["geocode"]})

    google.maps.event.addListener(this.autocomplete, "place_changed", () => {
      let crd = this.autocomplete.getPlace().geometry.location
      let location = [crd.lat(), crd.lng()].join(",")
      this.getForecast(location)
    });
  }

  getTemp(text) {
    return (text.match(/(\-?[0-9]+)/) || [])[1]
  }

  getForecast(location) {
    let DAILY_URL = `http://api.wunderground.com/api/${WUNDERGROUND_KEY}/hourly/q/EN/${location}o.json`
    let WEEKLY_URL = `https://api.wunderground.com/api/${WUNDERGROUND_KEY}/forecast/EN/q/${location}.json`

    fetch(DAILY_URL)
      .then(res => res.json())
      .then(forecast => forecast.hourly_forecast)
      .then(daily => {
          this.props.dispatch(setHourly(daily))
          localStorage["daily"] = JSON.stringify(daily)
      })

    fetch(WEEKLY_URL)
      .then(res => res.json())
      .then(forecast => forecast.forecast.txt_forecast.forecastday)
      .then(weekly => {
          this.props.dispatch(setWeekly(weekly))
          localStorage["weekly"] = JSON.stringify(weekly)
      })
  }

  render() {
    const { weekly, daily } = this.props
    console.log(this.props)

    return (
      <div>
        <input id="location" type="search" name="q" placeholder="Enter location" />
          <ul className="nav nav-tabs">
            <li><a data-toggle="tab" href="#daily">Daily</a></li>
            <li><a data-toggle="tab" href="#weekly">Weekly</a></li>
          </ul>
          <div className="tab-content">
            <div id="daily" className="tab-pane fade in active">
              {daily.map((hour, index) =>
                <div key={index}>
                  <p><strong>{hour.FCTTIME.civil}</strong></p>
                  <img src={hour.icon_url}/>
                  <p>{this.getTemp(hour.temp.metric)}&#8451;</p>
                </div>
              )}
            </div>
            <div id="weekly" className="tab-pane fade in active">
              {weekly.map((day, index) =>
                <div key={index}>
                  <p><strong>{day.title}</strong></p>
                  <img src={day.icon_url}/>
                  <p>{this.getTemp(day.fcttext_metric)}&#8451;</p>
                </div>
              )}
            </div>
          </div>
      </div>
    )
  }
}

function select(state) {
  return state;
}

export default connect(select)(Weather)
