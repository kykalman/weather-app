export const SET_WEEKLY = "SET_WEEKLY";
export const SET_DAILY = "SET_DAILY";

export function setWeekly(weekly) {
  return {
    type: SET_WEEKLY,
    weekly
  }
}

export function setHourly(daily) {
  return {
    type: SET_DAILY,
    daily
  }
}
