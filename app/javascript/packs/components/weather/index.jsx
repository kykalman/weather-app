import React from "react"
import { render } from "react-dom"
import { createStore } from "redux"
import { Provider } from "react-redux"
import Weather from "./weather"
import rootReducer from "./reducers"

let initialState = {
  weekly: localStorage["weekly"] != 'undefined' ? JSON.parse(localStorage["weekly"]) : [],
  daily: localStorage["daily"] != 'undefined' ? JSON.parse(localStorage["daily"]) : []
}
let store = createStore(rootReducer, initialState)

document.addEventListener("DOMContentLoaded", () => {
  render(
    <Provider store={store}>
      <Weather />
    </Provider>,
    document.getElementById("weather")
  )
})
