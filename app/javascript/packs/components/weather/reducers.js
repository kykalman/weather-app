import { SET_WEEKLY, SET_DAILY } from "./actions"

function rootReducer(state = {}, action) {
  switch(action.type) {
    case SET_WEEKLY:
      return Object.assign({}, state, {
        weekly: action.weekly
      });
    case SET_DAILY:
      return Object.assign({}, state, {
        daily: action.daily
      });
    default:
      return state;
  }
}

export default rootReducer;
